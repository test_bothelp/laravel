FROM composer:latest AS composer
FROM php:7.2-fpm

MAINTAINER Eliseenko Alexey <stalker7600@gmail.com>

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        libicu-dev \
        libpq-dev \
        libxpm-dev \
        libvpx-dev \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && pecl install mcrypt-1.0.1 \
    && docker-php-ext-enable mcrypt \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install -j$(nproc) intl \
    && docker-php-ext-install -j$(nproc) zip \
    && docker-php-ext-install -j$(nproc) pgsql \
    && docker-php-ext-install -j$(nproc) pdo_pgsql \
    && docker-php-ext-install -j$(nproc) exif \
    && docker-php-ext-configure gd \
        --with-freetype-dir=/usr/include/ \
        --with-jpeg-dir=/usr/include/ \
        --with-xpm-dir=/usr/lib/x86_64-linux-gnu/ \
    && pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis \
    && docker-php-ext-install pcntl

## Data
COPY . /var/www/html
WORKDIR /var/www/html

COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN composer install

RUN mkdir /var/www/html/storage/app/test
RUN chown -R www-data:www-data /var/www/html
RUN chmod 755 /var/www/html

# Optimizing
#RUN php artisan optimize
CMD ["php-fpm", "--allow-to-run-as-root"]

