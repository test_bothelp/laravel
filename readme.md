## Разворачивание проекта
1. Клонируем этот и configurations проект - `git clone <project>`
2. копируем `.env.example` в `.env`
3. Устанавливаем зависимости - `composer install --ignore-platform-reqs` либо идём в configurations и удаляем volume - TEST_VOLUME

# Docker
3. Генерим ключ приложения - `docker-compose up`
4. Запускаем миграции - `docker exec -it tz_test_1 php artisan migrate`
5. Можем покрутить кол-во процессов в `config/horizon.php` в processes
6. Запускаем генерацию событий - `docker exec -it tz_test_1 php artisan events:generate`
7. Можем перейти по адресу http://localhost:8080/horizon/dashboard и увидеть примерное время ожидания для очереди parse
8. Можно полистать консольку, увидеть на каких событиях падала/выполнялась не по порядку очередь
9. Проверить правильность выполнения события для этого пользователя в папке `storage/app/test` в файле `log_for_user{user_id}`

# Windows(не уверен)
3. Устанавливаем зависимости - `composer install`
4. Запускаем миграции - `php artisan migrate`
6. Генерим ключ приложения - `php artisan artisan events:generate`




