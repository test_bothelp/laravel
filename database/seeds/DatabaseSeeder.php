<?php

use App\Domain\Channel\Commands\UpdateChannel;
use App\Domain\Source\Commands\UpdateSource;
use App\Http\Controllers\ParseController;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Bus\DispatchesJobs;

class DatabaseSeeder extends Seeder
{

    use DispatchesJobs;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
    }
}
